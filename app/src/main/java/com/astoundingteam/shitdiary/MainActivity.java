package com.astoundingteam.shitdiary;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private String oldBristolString = "";
    private String newBristolString;
    private int bristolNumber=400;

    private DatePicker date;
    private TimePicker time;
    private LinearLayout timePickerBlock;
    private Calendar calendar = Calendar.getInstance();
    private TextView dropTime;

    private void updateDropTime() {
        String disp = DateFormat.getDateTimeInstance().format(calendar.getTimeInMillis());
        dropTime.setText(disp);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        date = (DatePicker) findViewById(R.id.date);
        // h/t O'one https://stackoverflow.com/a/34952495/948073
        date.init(
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH),
                new DatePicker.OnDateChangedListener() {
                    @Override
                    public void onDateChanged(DatePicker datePicker, int y, int m, int d) {
                        System.out.println("onDateChanged(tp," + y + "," + m + "," + d + ")");
                        calendar.set(Calendar.YEAR, y);
                        calendar.set(Calendar.MONTH, m);
                        calendar.set(Calendar.DAY_OF_MONTH, d);
                        updateDropTime();
                    }
                }
        );
        dropTime = (TextView) findViewById(R.id.dropTime);
        updateDropTime();
        time = (TimePicker) findViewById(R.id.tyme);
        timePickerBlock = (LinearLayout) findViewById(R.id.timePickerBlock);

        // bristolNumberView is to select a Bristol Stool Number.
        // This is a value between 1 and 7 inclusive
        // Since some cases may be seen as intermediate,
        // provision has been made to select to nearest hundredth.
        SeekBar bristolNumberView = (SeekBar) findViewById(R.id.bristolNumber);
        bristolNumberView.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                bristolNumber = progress;
                final TextView bsn = (TextView) findViewById(R.id.bsn);
                bsn.setText((CharSequence) String.format("%1.2f", (progress + 100.0) / 100.0));
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        // Normal use case is to enter shits as they come.
        // But if opportunity to use app occurs hours or days after a defecation,
        // editDropTime button makes the date and time pickers visible
        // so shits can be entered retroactively.
        final Button editDropTime = (Button) findViewById(R.id.changeTimeStamp);
        editDropTime.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View view) {
                TextView dropTimeView = (TextView) findViewById(R.id.dropTime);
                // find out visibility of time picker block
                int timePickerVisibility = timePickerBlock.getVisibility();
                // then toggle it
                int newVisibility;
                if (timePickerVisibility == LinearLayout.GONE) {
                    newVisibility = LinearLayout.VISIBLE;
                    editDropTime.setText("Done changing time");
                } else {
                    newVisibility = LinearLayout.GONE;
                    editDropTime.setText("Change time");
                }
                timePickerBlock.setVisibility(newVisibility);
            }
        });

        final Button saveData = (Button) findViewById(R.id.save);
        saveData.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                //String disp = DateFormat.getDateTimeInstance().format(calendar.getTimeInMillis());
                SeekBar shitSizeBar = (SeekBar) findViewById(R.id.shitSize);
                String shitSize = Integer.toString(shitSizeBar.getProgress());
                TextView notesView = (EditText) findViewById(R.id.notes);
                String notes = notesView.getText().toString();
                DbHelper dbHelper = new DbHelper(getApplicationContext());
                ContentValues values = new ContentValues();
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                String insertQuery = ""
                        + "insert into shits values ("
                        + "datetime("+calendar.getTimeInMillis()/1000+", 'unixepoch', 'localtime'),"
                        + Integer.toString(bristolNumber)+","
                        + shitSize+","
                        + "'"+notes+"'"
                        + ")";
                System.out.println(insertQuery);
                //db.execSQL();
                Intent i = new Intent(getApplicationContext(),SaveActivity.class);
                startActivity(i);

            }
        });

        time.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            public void onTimeChanged(TimePicker tp, int h, int m) {
                System.out.println("onTimeChanged(tp," + h + "," + m + ")");
                int y = calendar.get(Calendar.YEAR);
                int mo = calendar.get(Calendar.MONTH);
                int d = calendar.get(Calendar.DAY_OF_MONTH);
                calendar.set(Calendar.HOUR_OF_DAY, h);
                calendar.set(Calendar.MINUTE, m);
                // kludge?
                calendar.set(Calendar.YEAR, y);
                calendar.set(Calendar.MONTH, mo);
                calendar.set(Calendar.DAY_OF_MONTH, d);
                updateDropTime();
            }
        });
    }

}
